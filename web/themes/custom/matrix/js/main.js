/**
 * @file
 * Simple JavaScript hello world file.
 */
! function($) {
    $(document).ready(function() {
        var partners_slider = $('.partners');
        var toggle_item = $('.toggle-item');
        var toggle_item_team = $('.team-item');
        var dark_bg = $('.dark-background');

        function responsiveSlider() {
            var responsive_slider = $('.responsive');
            // Responsive slider.
            responsive_slider.slick({
                nextArrow: '<i class="fa fa-arrow-right slick-next-custom"></i>',
                prevArrow: '<i class="fa fa-arrow-left slick-prev-custom"></i>',
                infinite: true,
                speed: 300,
                responsive: [
                    {
                        breakpoint: 9999,
                        settings: "unslick"
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }
        function partnersSlider() {
            partners_slider.slick({
                arrows: false,
                infinite: true,
                dots: true,
                customPaging : function() {
                    return '<a><div class = "small-square"></div></a>';
                },
                slidesToShow: 5,
                slidesToScroll: 2,
                draggable: false,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 2
                        }
                    }
                ]
            });
        }

        responsiveSlider();
        partnersSlider();

        // Re-init slider when window resize.
        $(window).resize(function () {
            var responsive_slider = $('.responsive');
            responsive_slider.slick('unslick');
            responsiveSlider();
        });

        // We ajax request completed.
        $(document).ajaxComplete(function() {
            // Toggle items.
            $('.toggle-item').hover(function () {
                $(this).find('.dark-background').css({
                    'visibility': 'visible'
                });
            },function () {
                $(this).find('.dark-background').css({
                    'visibility': 'hidden'
                });
            });

            $('.responsive').slick({
                nextArrow: '<i class="fa fa-arrow-right slick-next-custom"></i>',
                prevArrow: '<i class="fa fa-arrow-left slick-prev-custom"></i>',
                infinite: true,
                speed: 300,
                responsive: [
                    {
                        breakpoint: 9999,
                        settings: "unslick"
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

        });
        // Toggle items.
        toggle_item.hover(function () {
           $(this).find(dark_bg).css({
               'visibility': 'visible'
           });
        },function () {
            $(this).find(dark_bg).css({
                'visibility': 'hidden'
            });
        });

        toggle_item_team.hover(function () {
            $(this).find('.social-link').css({
                'visibility': 'visible'
            });
            $(this).find('h2').addClass('active');
        },function () {
            $(this).find('.social-link').css({
                'visibility': 'hidden'
            });
            $(this).find('h2').removeClass('active');
        });

        $(".news-item, .mini-news-item").hover(function () {
            $(this).find('.toggle-news-mini a').css({
                'visibility': 'visible'
            });
            $(this).find('.toggle-item-news a').css({
                'visibility': 'visible'
            });
            $(this).find('h3 a').addClass('active');
        },function () {
            $(this).find('.toggle-news-mini a').css({
                'visibility': 'hidden'
            });
            $(this).find('.toggle-item-news a').css({
                'visibility': 'hidden'
            });
            $(this).find('h3 a').removeClass('active');
        });

        // Shopping cart.
        // Increment item.
        $(".button-green").click(function () {
            var cart_item = $(".cart-item");
            var count_item = parseInt(cart_item.text()) + 1;
            cart_item.html(count_item);
        });

        // Decrement item.
        $(".fa-shopping-cart").click(function () {
            var cart_item = $(".cart-item");
            if ( parseInt(cart_item.text()) > 0 ) {
                var count_item = parseInt(cart_item.text()) - 1;
                cart_item.html(count_item);
            }
        });

        // Slider.
        $('.single-item').slick({
            arrows: false,
            autoplay: true,
            infinite: true,
            dots: true,
            customPaging : function() {
                return '<a><div class = "small-square"></div></a>';
            },
            initialSlide: 1,
            draggable: true
        });

        $('.autoplay').slick({
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            fade: true,
            cssEase: 'linear'
        });

        // Header.
        var currentScrollTop = $(window).scrollTop();

        $(window).scroll(function(){
            currentScrollTop = $(window).scrollTop();
            if (currentScrollTop != 0) {
                $('.header-top').addClass('header-top-active');
            } else {
                $('.header-top').removeClass('header-top-active');
            }
        });


        // Main menu.
        $('.fa-times').click(function () {
            $('.header-nav').slideUp();
        });
        $('.fa-bars').click(function () {
            var header_nav = $('.header-nav');
            header_nav.addClass('open-menu');
            header_nav.slideDown();
        });
        $('.main-menu a').click(function () {
            $('.open-menu').slideUp();
        });

        // Scrolling
        $(".main-menu a, footer a[href='#myPage'], footer a[href='#recent-works']").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {

                event.preventDefault();

                var hash = this.hash;

                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function(){
                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });

        $(".fa-search").click(function () {
            $(".search-block").fadeToggle('slow');
        });

        $(".main-menu").changeActiveNav();

    });
}(jQuery);
