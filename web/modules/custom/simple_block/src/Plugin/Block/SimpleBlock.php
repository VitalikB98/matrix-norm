<?php

namespace Drupal\simple_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Provides a Simple Block.
 *
 * @Block(
 *  id = "simply_block",
 *  admin_label = @Translation("Simple block"),
 * )
 */
class SimpleBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['name'])) {
      $name = $config['name'];
    }
    else {
      $name = $this->t('to no one');
    }
    return array(
      '#markup' => $this->t('Simple Block! Build: @name', array(
        '@name' => $name,
      )),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['simply_block_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Who'),
      '#description' => $this->t('Who do you want to say hello to?'),
      '#default_value' => isset($config['name']) ? $config['name'] : '',
    );

    $form['height'] = array(
      '#type' => 'number',
      '#title' => $this->t('height'),
      '#default_value' => isset($config['height']) ? $config['height'] : '',
    );

    $form['width'] = array(
      '#type' => 'number',
      '#title' => t('width'),
      '#default_value' => isset($config['width']) ? $config['width'] : '',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = \Drupal::config('simply_block_name.settings');
    return array(
      'name' => $default_config->get('simply.name'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['name'] = $form_state->getValue('simply_block_name');
    $this->configuration['height'] = $form_state->getValue('height');
    $this->configuration['width'] = $form_state->getValue('width');

  }

}
