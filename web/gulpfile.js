/**
 * Created by user on 03.04.17.
 */
var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
    return gulp.src('themes/custom/matrix/scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('themes/custom/matrix/css'))
});

gulp.task('watch', ['sass'], function () {
    gulp.watch('themes/custom/matrix/scss/**/*.scss', ['sass']);
});